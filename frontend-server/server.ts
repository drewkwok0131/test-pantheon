import express from 'express'
import path from 'path'
import { print } from 'listening-on'
import { env } from './env'

let app = express()

if (!env.SESSION_SECRET) {
	throw new Error('missing SESSION_SECRET in env')
}

app.use(express.static(path.resolve('../frontend/build')))
app.get('/', (req, res) => {
	res.send(path.resolve('../frontend/build/index.html'))
})

app.use((req, res) => {
	res.sendFile(path.resolve(path.join('public', '404.html')))
})

let port = env.PORT

app.listen(port, () => {
	print(port)
})

