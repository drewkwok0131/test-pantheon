import { config } from "dotenv"

config();

export let env = {
    SESSION_SECRET : process.env.SESSION_SECRET,
    PORT: +process.env.PORT! || 3100,
    NODE_ENV:process.env.NODE_ENV || "development",
}
