import fetch from "node-fetch";
import { env } from './env.js';
import crypto from "crypto"

// Key Provided by Storyblocks
const publicKey = "test_6e87a0e88f7f11ae6a40d1082893d136eecf5b17cd48c4ce5eba8c167ec"
const privateKey = env.STORYBLOCKS_API_PRIVATE_KEY;


export async function searchFromStoryblocksAPI(userInput, resultPerPage, page) {

  try {
    // HMAC generation
    let resource = '/api/v2/images/search';
    let expires = Math.floor(Date.now() / 1000) + 100;
    let hmacBuilder = crypto.createHmac('sha256', privateKey + expires);
    hmacBuilder.update(resource);
    let hmac = hmacBuilder.digest('hex');

    // project_id, user_id are required for searching 
    let project_id = "1"
    let user_id = "1"
    let keywords = userInput.split(/[ ,]+/).join(',')

    let url = `https://api.graphicstock.com/api/v2/images/search?APIKEY=${publicKey}&EXPIRES=${expires}&HMAC=${hmac}&project_id=${project_id}&user_id=${user_id}&keywords=${keywords}&orientation=portrait&page=${page}&results_per_page=${resultPerPage}&sort_order=ASC`

    let response = await fetch(url);
    let apiResult = await response.json();

    let imgList = []

    for (let i = 0; i < resultPerPage; i++) {
      let image_ID = apiResult.results[i].id
      let thumbnails = apiResult.results[i].thumbnail_url
      let preview = apiResult.results[i].preview_url
      let title = apiResult.results[i].title
      let source = "Storyblocks"
      let tags = null

      let result = {
        image_ID,
        thumbnails,
        preview,
        title,
        source,
        tags
      }
      imgList.push(result)
    }
    return imgList

  } catch (error) {
    return { message: "failed to fetch data" }
  }

}