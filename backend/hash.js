import bcrypt from 'bcryptjs'

//user password hash before saving to database

let ROUND = 12

export async function hashPassword(password) {
  let digest = await bcrypt.hash(password, ROUND)
  return digest
}

export async function comparePassword(o) {
  let isMatch = await bcrypt.compare(o.password, o.password_hash)
  return isMatch
}