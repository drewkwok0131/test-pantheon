import { userService } from './service.js'
import pkg from 'permit';
const { Bearer } = pkg;

const permit = new Bearer({
    query: "access_token"
})

export function requireJWTPayload(req) {
    let token
    try {
        token = permit.check(req);
    } catch (error) {
        throw new HttpError('Invalid Bearer Token', 401)
    }
    let payload = userService.decodeToken(token)
    return payload
}