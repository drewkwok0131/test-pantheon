import { config } from "dotenv"

config();

if (!process.env.SESSION_SECRET) {
    throw new Error('missing SESSION_SECRET in env')
}
if (!process.env.UNSPLASH_API_KEY) {
    throw new Error('missing UNSPLASH_API_KEY in env')
}
if (!process.env.PIXABAY_API_KEY) {
    throw new Error('missing PIXABAY_API_KEY in env')
}
if (!process.env.STORYBLOCKS_API_PRIVATE_KEY) {
    throw new Error('missing STORYBLOCKS_API_PRIVATE_KEY in env')
}

export let env = {
    SESSION_SECRET: process.env.SESSION_SECRET,
    PORT: +process.env.PORT || 8080,
    NODE_ENV: process.env.NODE_ENV || "development",
    JWT_SECRET:process.env.JWT_SECRET,
    DB_NAME:process.env.DB_NAME,
    DB_USERNAME:process.env.DB_USERNAME,
    DB_PASSWORD:process.env.DB_PASSWORD,
    UNSPLASH_API_KEY: process.env.UNSPLASH_API_KEY,
    PIXABAY_API_KEY: process.env.PIXABAY_API_KEY,
    STORYBLOCKS_API_PRIVATE_KEY: process.env.STORYBLOCKS_API_PRIVATE_KEY
}
