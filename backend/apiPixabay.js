import fetch from "node-fetch";
import { env } from './env.js'

let apiKey = env.PIXABAY_API_KEY

export async function searchFromPixabayAPI(userInput, resultPerPage, page) {

    //fit the specific requirment of input keyword for PIXABAY
    let keywords = userInput.split(/[ ]+/).join('+')

    try {
        let url = `https://pixabay.com/api/?key=${apiKey}&q=${keywords}&image_type=photo&per_page=${resultPerPage}&page=${page}`;
        let response = await fetch(url);
        let apiResult = await response.json();    
    
        let imgList = []
    
        for (let i = 0; i < resultPerPage; i++) {
            let image_ID = apiResult.hits[i].id
            let thumbnails = apiResult.hits[i].previewURL
            let preview = apiResult.hits[i].webformatURL
            let title = null
            let source = "Pixabay"
            let tagsString = apiResult.hits[i].tags
            let tags = tagsString.split(',')
    
            let result = {
                image_ID,
                thumbnails,
                preview,
                title,
                source,
                tags
            }
            imgList.push(result)
        }
        return imgList
        
     } catch (error) {
         return {message : "failed to fetch data"}        
     }

}



