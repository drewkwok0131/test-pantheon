/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export async function up(knex) {
    if(await knex.schema.hasTable('users')) return
    await knex.schema.createTable('users', table =>{
        table.increments('id')
        table.string('username', 64).notNullable().unique()
        table.string('password_hash', 60).notNullable()
        table.timestamps(false, true)
    })
  
};

/**
 * @param { import("knex").Knex } knex
 * @returns { Promise<void> }
 */
export async function down(knex) {
    return knex.schema.dropTable('users')  
};
