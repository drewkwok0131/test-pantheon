document.querySelector('#search-req-form').addEventListener('submit', async function(event){
    try {
        event.preventDefault()

        const form = event.target
        const formObject = {}

        formObject['userInput'] = form.userInput.value

        console.log("formObject: ", formObject);

        const res = await fetch('/search-req', {
            method:"POST",
            headers:{
                "Content-Type":"application/json"
            },
            body: JSON.stringify(formObject)
        })
        const result = await res.json()
        console.log("result: ", result);
        document.querySelector('#result').innerHTML = JSON.stringify(result)
    } catch (error) {
        console.error;        
    }
})