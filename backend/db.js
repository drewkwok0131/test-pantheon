import Knex from "knex"
import configs from "./knexfile.js"
import { env } from "./env.js"

let mode = env.NODE_ENV
let config = configs[mode]

export let knex = Knex(config)