import { env } from "./env.js";

/**
 * @type { Object.<string, import("knex").Knex.Config> }
 */
const config = {

  development: {
    client: 'postgresql',
    connection: {
      database: env.DB_NAME,
      user:     env.DB_USERNAME,
      password: env.DB_PASSWORD
    },
    pool: {
      min: 2,
      max: 10
    },
    migrations: {
      tableName: 'knex_migrations'
    }
  }

};

export default config;
