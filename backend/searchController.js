import { Router } from "express";
import { HttpError } from "./httpError.js"
import { searchFromUnsplash } from "./apiUnsplash.js";
import { searchFromPixabayAPI } from "./apiPixabay.js";
import { searchFromStoryblocksAPI } from "./apiStoryblocks.js"

export class SearchController {
    router = Router()
    constructor() {
        this.router.post('/search-req', this.searchReq)
    }

    // handle data from frontend user input
    searchReq = async (req, res) => {
        try {
            let userInput = req.body.data.userInput
            let page = 1
            let resultPerPage = 5
    
            if (!userInput) throw new HttpError('missing userInput in request body', 400)
    
            //fetch 3 api source asynchronously
            let [resultOne, resultTwo, resultThree] = await Promise.all(
                [searchFromUnsplash(userInput, resultPerPage, page),
                searchFromPixabayAPI(userInput, resultPerPage, page),
                searchFromStoryblocksAPI(userInput, resultPerPage, page)
                ])
    
            if(resultOne.message && resultTwo.message && resultThree.message){
                res.json({message : "no result can be found"})
                return
            }
            if(resultOne.message ){
                resultOne = []
            }
            if(resultTwo.message ){
                resultTwo = []
            }
            if(resultThree.message ){
                resultThree = []
            }
    
            let totalResult = resultOne.concat(resultTwo).concat(resultThree)
    
            res.json(totalResult)
            
        } catch (error) {
            return res.status(500).json('failed to search photo')          
        }  
    }
}

