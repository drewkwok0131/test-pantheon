import { Router } from "express";

//Check users' authentication and return JWT token to controller
export class UserController {
    router = Router()

    constructor(userService) {
        this.userService = userService
        this.router.post('/login', this.login)
        this.router.post('/register', this.register)
    }

    login = async (req, res) => {
        try {
            let { username, password } = req.body
            if (!username) return res.status(400).json('missing username in request body')
            if (!password) return res.status(400).json('missing password in request body')
            let user = { username, password }
            let token = await this.userService.login(user)

            return res.json({ token })
        } catch (error) {
            return res.status(500).json('failed to login')
        }
    }

    register = async (req, res) => {
        try {
            let { username, password } = req.body
            if (!username) return res.status(400).json('missing username in request body')
            if(username.length < 3) return res.status(400).json('username should have at least 3 characters')
            if (!password) return res.status(400).json('missing password in request body')
            if(password.length < 8) return res.status(400).json('password should have at least 8 characters or numbers')
            let user = { username, password }
            let token = await this.userService.register(user)

            return res.json({ token })
        } catch (error) {
            return res.status(500).json('failed to register')
        }
    }
}