import { createApi } from "unsplash-js";
import fetch from "node-fetch";
import { env } from './env.js'


global.fetch = fetch;
const unsplash = createApi({
  accessKey: env.UNSPLASH_API_KEY
});

export async function searchFromUnsplash(userInput, resultPerPage, page) {
  try {
    let apiResult = await unsplash.search.getPhotos({
      query: userInput,
      page: page,
      perPage: resultPerPage,
      orientation: 'portrait',
    });
  
    if (apiResult.response.total === 0) {
      console.log("no result can be found in unsplash");
      return {message : "failed to fetch data"}
    }
  
    let imgList = []
  
    for (let i = 0; i < resultPerPage; i++) {
      let image_ID = apiResult.response.results[i].id
      let thumbnails = apiResult.response.results[i].urls.thumb
      let preview = apiResult.response.results[i].urls.regular
      let title = apiResult.response.results[i].alt_description
      let source = "Unsplash"
      let tagsList = apiResult.response.results[i].tags

      let tags = []
      for (let j = 0; j < tagsList.length; j++) {
        tags.push(tagsList[j].title)
      }
      let result = {
        image_ID,
        thumbnails,
        preview,
        title,
        source,
        tags
      }
      imgList.push(result)
    }
    return imgList
    
  } catch (error) {
    return {message : "failed to fetch data"}
    
  }

}