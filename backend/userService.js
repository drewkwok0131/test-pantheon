import { comparePassword, hashPassword } from "./hash.js";
import { HttpError } from "./httpError.js";
import jwt from 'jwt-simple';
import { jwtConfig } from "./jwt.js";


export class UserService {

    constructor(knex) {
        this.knex = knex
    }

    //create and decode JWT token to login user
    createToken(id) {
        let payload = { id: id }
        let token = jwt.encode(payload, jwtConfig.jwtSecret)
        return token
    }

    decodeToken(token) {
        try {
            let payload = jwt.decode(token, jwtConfig.jwtSecret)
            return payload
        } catch (error) {
            throw new HttpError("invalid jwt token", 401)
        }

    }

    async register(registerUser) {
        let checkDuplicated = await this.knex
        .select('id')
        .from('users')
        .where({ username: registerUser.username })
        if(checkDuplicated.length === 1){
            return {error: "username have been registered, please use anther one"}
        }

        let row = await this.knex.insert({
            username: registerUser.username,
            password_hash: await hashPassword(registerUser.password)
        }).into('users').returning('id')

        let id = row[0].id
        let token = this.createToken(id)

        return token
    }

    async login(loginUser) {

        let row = await this.knex.
            select('id', 'password_hash')
            .from('users').
            where({ username: loginUser.username })

        if (row.length === 0) {
            return {error: "user not found"}
        }

        let isMatch = await comparePassword({
            password: loginUser.password,
            password_hash: row[0].password_hash
        })
        if (!isMatch) {
            return {error: "Wrong username or password"}
        }
        let token = this.createToken(row[0].id)
        return token
    }


}