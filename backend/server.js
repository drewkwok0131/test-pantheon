import express from 'express'
import path from 'path'
import session from 'express-session'
import { print } from 'listening-on'
import { env } from './env.js'
import { SearchController } from './searchController.js'
import { userService} from './service.js'
import { UserController} from './userController.js'
import cors from 'cors'

let app = express()
app.use(express.json())
app.use(cors())

if (!env.SESSION_SECRET) {
    throw new Error('missing SESSION_SECRET in env')
}

app.use(session({
    secret: env.SESSION_SECRET,
    resave: true,
    saveUninitialized: true,
}))

const userController = new UserController(userService)
const searchController = new SearchController()

app.use(userController.router)
app.use(searchController.router)

app.use(express.static('public'))
app.use((req, res) => {
    res.sendFile(path.resolve(path.join('public', '404.html')))
})

let port = env.PORT

app.listen(port, () => {
    print(port)
})
