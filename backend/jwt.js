import { env } from './env.js';

export let jwtConfig = {
    jwtSecret: env.JWT_SECRET,
    jwtSession: {
        session: false
    }
}