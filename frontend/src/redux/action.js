
export const LOG_OUT = "LOG_OUT"
export const LOGIN = "LOGIN"
export const REGISTER = "REGISTER"


export function loginAction(result) {
    console.log('loginAction(result): ', result);
    return { type: LOGIN , result}
}
export function registerAction(result){
    console.log('registerAction(result): ', result);
    return { type: REGISTER , result}
}

export function logoutAction() {
    return { type: LOG_OUT }
}