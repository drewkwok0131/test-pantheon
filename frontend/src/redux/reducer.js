import jwtDecode from 'jwt-decode'
import { LOG_OUT, LOGIN, REGISTER } from './action'

function loadUserFromToken(token) {
    if (!token) return null
    try {
        let payload = jwtDecode(token)
        return { id: payload.id, token }
    } catch (error) {
        console.log('failed to decode JWT', error);
        return null
    }
}

function initialState() {
    let token = localStorage.getItem('token')
    return {
        user: loadUserFromToken(token),
        registerResult: { type: 'idle' },
        loginResult: { type: 'idle' }
    }
}

export function rootReducer(
    state = initialState(),
    action) {
    switch (action.type) {
        case LOG_OUT:
            console.log("LOG_OUT & action.result: ", action.result);
            return {
                user: null,
                registerResult: { type: 'idle' },
                loginResult: { type: 'idle' }
            }
        case LOGIN:
            console.log("LOGIN & action.result: ", action.result);
            return {
                ...state,
                loginResult: action.result,
                user: loadUserFromToken(action.result.type === 'success' ?
                    action.result.token : null)
            }
        case REGISTER:
            console.log("REGISTER & action.result: ", action.result);
            return {
                ...state,
                registerResult: action.result,
                user: loadUserFromToken(action.result.type === 'success' ?
                    action.result.token : null)
            }
        default:
            return state
    }

}