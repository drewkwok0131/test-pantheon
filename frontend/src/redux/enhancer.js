import { applyMiddleware, compose } from 'redux'
import logger from 'redux-logger'
import thunk from 'redux-thunk'


const composeEnhancer = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

export let rootEnhancer = composeEnhancer(
    applyMiddleware(logger), 
    applyMiddleware(thunk),
    )