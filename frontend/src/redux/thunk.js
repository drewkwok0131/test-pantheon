import { loginAction, logoutAction, registerAction } from "./action"
import { env } from '../env'


export function logoutThunk() {
    return async (dispatch) => {
        console.log(localStorage);
        localStorage.removeItem('token')
        dispatch(logoutAction())
    }
}

export function loginThunk(data) {
    return async (dispatch) => {
        try {
        console.log(data);
        let { username, password } = data
            console.log({ username, password });
            let res = await fetch(`${env.API_ORIGIN}/login`, {
                method: 'POST',
                headers: {
                    "Content-Type": "application/json"
                },
                body: JSON.stringify({ username, password })
            })
            let result = await res.json()
            console.log('result received from thunk: ', result);
            if(result.token.error){
                dispatch(loginAction({
                    type: 'fail',
                    message: result.token.error
                }))
            } else {
                localStorage.setItem('token', result.token)
                dispatch(loginAction({
                    type: 'success', 
                    token: result.token
                }))
            }
        } catch (error) {
              return {error}
        }
    }
}

export function registerThunk({username, password}){
    return async (dispatch) =>{
        try {
            
        let res = await fetch(`${env.API_ORIGIN}/register`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({ username, password })
        })
        let result = await res.json()
        console.log('result received from thunk: ', result);
        if(result.token.error){
            dispatch(registerAction({
                type : 'fail',
                message : result.token.error
            }))
        } else {
            localStorage.setItem('token', result.token)
            dispatch(registerAction({
                type: 'success', 
                token: result.token
            }))
        }            
        } catch (error) {
            return {error}            
        }
    }
}