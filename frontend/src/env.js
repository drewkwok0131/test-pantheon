
if (!process.env.REACT_APP_API_ORIGIN) {
    throw new Error('missing REACT_APP_API_ORIGIN')
}

export let env = {
    API_ORIGIN: process.env.REACT_APP_API_ORIGIN,
}




