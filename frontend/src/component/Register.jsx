import styles from "./Register.module.css"
import React, { useEffect, useState } from 'react'
import { useForm } from "react-hook-form"
import { useDispatch, useSelector } from "react-redux"
import {registerThunk} from '../redux/thunk'


function Register() {
    const [message, setMessage] = useState("")
    const dispatch = useDispatch()
    const { register, handleSubmit} = useForm({
        defaultValues: {
            username: '',
            password: '',
            confirm_password: '',
        }
      }        
    )

    let errorMessage = {
        username : "username should be at least 3 characters",
        password : "password should constain at least 8 characters or number",
        confirm_password: "password doesn't match"   
    }

    const onSubmit = (data) => {
        console.log(data)
        if(data.username.length < 3){
            setMessage(errorMessage.username)
        }else if(data.password.length < 8){
            setMessage(errorMessage.password)
        }else if(data.confirm_password !== data.password){
            setMessage(errorMessage.confirm_password)
        }
        dispatch(registerThunk(data))
    }

    //show error message to user after checking database 
    const isRegistered = useSelector(state => state)
    useEffect(()=>{
        if(isRegistered.registerResult.type === 'fail'){
            setMessage(isRegistered.registerResult.message)
        }
    },[isRegistered])

    return (
        <div className={styles.container}>
            <div className={styles.headline}>Registration</div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <label>Username</label>
                    <div>
                        <input {...register('username', { required: true })} />
                    </div>
                </div>

                <div>
                    <label>Password</label>
                    <div>
                        <input type="password" {...register('password', { required: true })} />
                    </div>
                </div>

                <div>
                    <label>Confirm Password</label>
                    <div>
                        <input type="password" {...register('confirm_password', { required: true })} />
                    </div>
                </div>

                <div>
                    <button type="submit">Submit</button>
                </div>
            </form>
            <div>{message}</div>
        </div>
    )
}

export default Register