import { Link } from 'react-router-dom';
import { logoutThunk } from '../redux/thunk'
import { useDispatch } from 'react-redux';
import styles from "./TopBar.module.css"

function TopBar() {
    const dispatch = useDispatch()

    const logout = () => {
        dispatch(logoutThunk())
    }
    return (
        <div className={styles.container}>
            <div className={styles.box}><Link to='/' style={{ textDecoration: 'none', color: 'black' }}>Register</Link></div>
            <div className={styles.box}><Link to='/login' style={{ textDecoration: 'none', color: 'black' }}>Login </Link></div>
            <div className={styles.box}><Link to='/canvas' style={{ textDecoration: 'none', color: 'black' }}>CANVAS</Link></div>
            <div className={styles.box}><Link to='/search' style={{ textDecoration: 'none', color: 'black' }}>Search API</Link></div>
            <div className={styles.box} onClick={logout}>Logout</div>
        </div>
    )

}

export default TopBar