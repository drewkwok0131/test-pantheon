import { useEffect, useState } from "react"

function CanvasVideo() {

  //scene data
  let mediaToLoad = [{
    index: 0,
    sentence: "This is a simple Javascript test",
    media: "https://miro.medium.com/max/1024/1*OK8xc3Ic6EGYg2k6BeGabg.jpeg",
    duration: 3
  }, {
    index: 1,
    sentence: "Here comes the video!",
    media: "https://media.gettyimages.com/videos/goodlooking-young-woman-in-casual-clothing-is-painting-in-workroom-video-id1069900546",
    duration: 5
  }]

  const [canvas, setCanvas] = useState(null)

  useEffect(() => {
    if (!canvas) return

    //define the size of canvas
    let w = 800
    let h = (w / 16) * 9
    canvas.width = w
    canvas.height = h
    const ctx = canvas.getContext('2d')

    // make the image/video source fit to 16:9 format 
    function calcSize(srcW, srcH) {
      let ratioW = srcW / w
      let ratioH = srcH / h
      let destW, destH
      let dx, dy
      if (ratioW < ratioH) {
        destW = srcW / ratioW
        destH = srcH / ratioW
        dx = 0
        dy = (h - destH) / 2
      } else {
        destW = srcW / ratioH
        destH = srcH / ratioH
        dx = (w - destW) / 2
        dy = 0
      }
      return { dx, dy, w: destW, h: destH }
    }

    // draw image to canvas
    async function drawImage(src) {
      let image = await loadImage(src)
      let size = calcSize(image.naturalWidth, image.naturalHeight)
      ctx.drawImage(image, size.dx, size.dy, size.w, size.h)
    }

    function loadImage(src) {
      console.log('load image:', src)
      return new Promise((resolve, reject) => {
        let image = document.createElement('img')
        image.onload = () => {
          console.log('loaded image:', src)
          resolve(image)
        }
        image.onerror = reject
        image.src = src
      })
    }

    // draw video to canvas
    async function drawVideo(src) {
      return new Promise((resolve, reject) => {
        let video = document.createElement('video')
        let isPlayingVideo = false
        let timer
        let size

        function draw() {
       
          if (!isPlayingVideo) {
            return
          }
          ctx.drawImage(video, size.dx, size.dy, size.w, size.h)
          timer = requestAnimationFrame(draw)
        }
        video.onloadedmetadata = () => {
          size = calcSize(video.videoWidth, video.videoHeight)
        }

        video.onloadeddata = () => {
          isPlayingVideo = true
          draw()
        }
        video.onended = () => {
          isPlayingVideo = false
          cancelAnimationFrame(timer)
          resolve()
        }
        video.onerror = reject
        video.preload = 'auto'
        video.muted = true
        video.src = `${src}#t=5,10`
        video.play()
      })  
    }

    //To make image last 3s inside canvas
    function wait(ms) {
      console.log('wait:', ms)
      return new Promise(resolve =>
        setTimeout(() => {
          console.log('waited:', ms)
          resolve()
        }, ms),
      )
    }

    //run the application one by one
    async function main() {
      await drawImage(mediaToLoad[0].media)
      await wait(3000)
      await drawVideo(mediaToLoad[1].media)
    }
    main()

  }, [canvas])

  return (
    <div>
      <canvas
        ref={e => {
          if (e !== canvas) {
            setCanvas(e)
          }
        }}
      />
    </div>
  )
}

export default CanvasVideo