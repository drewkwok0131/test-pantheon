import styles from "./Login.module.css"
import { useEffect, useState } from "react"
import { useForm } from "react-hook-form"
import { useDispatch, useSelector } from "react-redux"
import { loginThunk } from "../redux/thunk"

function Login() {
    
    const [message, setMessage] = useState("")
    const dispatch = useDispatch()
    const { register, handleSubmit } = useForm({
        defaultValues: {
            username: '',
            password: ''
        }
    })

    const onSubmit = (data) => {
        console.log(data)
        dispatch(loginThunk(data))
    }

    const isLoggedin = useSelector(state => state)
    useEffect(()=>{
        if(isLoggedin.loginResult.type === 'fail'){
            setMessage(isLoggedin.loginResult.message)
        }
    },[isLoggedin])

    return (
        <div className={styles.container}>
            <div className={styles.headline}>Login</div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <label>Username</label>
                    <div>
                        <input {...register("username", { required: true })} />
                    </div>
                </div>
                <div>
                    <label>Password</label>
                    <div>
                        <input type="password" {...register("password", { required: true })} />
                    </div>
                </div>

                <div>
                    <button type="submit">Submit</button>
                </div>
            </form>
            <div>{message}</div>

        </div>
    )

}

export default Login