import { useForm } from "react-hook-form"
import styles from "./Search.module.css"
import { env } from '../env'
import { useState } from "react"


function Search() {

    const [result ,setResult] = useState('')
    const { register, handleSubmit } = useForm({
        defaultValues: {
            userInput: ''
        }
    })

    const onSubmit = async (data) => {
        console.log(data)
        let res = await fetch(`${env.API_ORIGIN}/search-req`, {
            method: 'POST',
            headers: {
                "Content-Type": "application/json"
            },
            body: JSON.stringify({data})
        })
        let result = await res.json()        
        console.log('result: ', result);
        setResult(JSON.stringify(result))
    }


    return (
        <div className={styles.container}>
            <div>
            <div className={styles.headline}>Type Keywords to Search Photo URL</div>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div>
                    <div>
                        <input {...register("userInput", { required: true })} />
                    </div>
                </div>

                <div>
                    <button type="submit">Submit</button>
                </div>
            </form>
            <div>{result}</div>
            </div>
        </div>
    )

}

export default Search