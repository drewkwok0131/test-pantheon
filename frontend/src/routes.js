import CanvasVideo from './component/CanvasVideo';
import Register from './component/Register';
import Login from './component/Login';
import Search from './component/Search';
import { Route, Routes, Navigate } from 'react-router-dom';
import { useSelector } from 'react-redux';
import TopBar from './component/TopBar';



function RouteLinks() {

  const isLoggedin = useSelector(state => state.user)



  return (
    <div>
      <TopBar />
      <Routes>
        <Route path='/' element={!isLoggedin ? <Register /> : <Navigate to="/canvas" ></Navigate>} />
        <Route path='/login' element={!isLoggedin ? <Login /> : <Navigate to="/canvas" ></Navigate>} />
        <Route path='/canvas' element={isLoggedin ? <CanvasVideo /> : <Navigate to="/login" ></Navigate>} />
        <Route path='/search' element={isLoggedin ? <Search /> : <Navigate to="/login" ></Navigate>} />
      </Routes>

    </div>


  );
}

export default RouteLinks;